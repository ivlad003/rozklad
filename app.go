package main

import (
    "github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"encoding/json"
	"io/ioutil"
)


var dbString = "alex_test:STsvaH4Dh9@tcp(node.it-data.co.ua:3306)/alex_test"

func main() {
	r := gin.Default()
	r.POST("/query/json", func(c *gin.Context) {
		x, _ := ioutil.ReadAll(c.Request.Body)
		c.JSON(200, gin.H{
			"message": getJsonFromRows(string(x)),
		})
	})
	r.POST("/query/byte", func(c *gin.Context) {
		x, _ := ioutil.ReadAll(c.Request.Body)
		c.JSON(200, gin.H{
			"message": getBytesJsonFromRows(string(x)),
		})
	})
	r.Run(":9898")
}

func getJsonFromRows(query string) string  {
	db, err := sql.Open("mysql", dbString)
	fck(err)
		rows, err := db.Query(query)
		columnNames, err := rows.Columns()
		fck(err)
		defer db.Close()
		defer rows.Close()
		var maps []map[string]string
		for rows.Next() {
			cv, err := rowMapString(columnNames, rows)
			fck(err)
			maps = append(maps, cv)
		}
		jsonString, err := json.Marshal(maps)
		fck(err)
		return BytesToString(jsonString)
}

func getBytesJsonFromRows(query string) []byte  {
	db, err := sql.Open("mysql", dbString)
	fck(err)
	defer db.Close()
	rows, err := db.Query(query)
	columnNames, err := rows.Columns()
	fck(err)
	defer rows.Close()
	var maps []map[string]string
	for rows.Next() {
		cv, err := rowMapString(columnNames, rows)
		fck(err)
		maps = append(maps, cv)
	}
	jsonString, err := json.Marshal(maps)
	fck(err)
	return jsonString
}