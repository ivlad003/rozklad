package main

import (
	"database/sql"
	"fmt"
)

type TestItem struct {
	NameOfStationID int
	RouteID         int
	NumberOfStation int
	Name            string
	Interval        string
}

func InitDB(filepath string) *sql.DB {
	db, err := sql.Open("sqlite3", filepath)
	if err != nil {
		panic(err)
	}
	if db == nil {
		panic("db nil")
	}
	return db
}

func ReadItem(db *sql.DB) []TestItem {

	sql_readall2 := `SELECT name FROM sqlite_master WHERE type='table'`
	rows2, err := db.Query(sql_readall2)
	if err != nil {
		panic(err)
	}
	defer rows2.Close()

	var names []string
	for rows2.Next() {
		var name string
		err2 := rows2.Scan(&name)
		if err2 != nil {
			panic(err2)
		}
		names = append(names, name)
	}
	fmt.Println(names)
	sql_readall := `select NameOfStationID, RouteID, Name, NumberOfStation, Interval from station`
	rows, err := db.Query(sql_readall)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var result []TestItem
	for rows.Next() {
		item := TestItem{}
		err2 := rows.Scan(&item.NameOfStationID, &item.RouteID, &item.Name, &item.NumberOfStation, &item.Interval)
		if err2 != nil {
			panic(err2)
		}
		result = append(result, item)
	}

	return result
}
